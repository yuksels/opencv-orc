#-------------------------------------------------
#
# Project created by QtCreator 2019-09-30T10:21:42
#
#-------------------------------------------------

QT       += core gui
QT += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OcrBasedBillRecognize
TEMPLATE = app


include("/home/cam/Downloads/opencv-3.4.3/opencv_library_path.pri")
SOURCES += main.cpp\
        mainwindow.cpp

INCLUDEPATH += /usr/include/opencv
LIBS += -L/usr/local/lib -lopencv_dnn




HEADERS  += mainwindow.h

FORMS    += mainwindow.ui


INCLUDEPATH += /usr/local/include/tesseract
INCLUDEPATH += /usr/local/include/leptonica
LIBS += -ltesseract
