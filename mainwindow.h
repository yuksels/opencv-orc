#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<string>
#include <QtSql>
#include <QtDebug>
#include <QRect>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlQueryModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	void function(QString);
	void connectDatabase();
	void listele();

private slots:


	void on_pshBtnSelectImage_clicked();

	void on_pshBtnConnectDatabase_clicked();

private:
	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
