#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<opencv2/opencv.hpp>
#include<string>
#include<tesseract/baseapi.h>
#include<leptonica/allheaders.h>
#include<iostream>
#include<QFileDialog>
#include<QMessageBox>
#include<QPixmap>

using namespace cv;
using namespace std;
QSqlDatabase mydb = QSqlDatabase::addDatabase("QSQLITE");
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);



}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::function(QString path)
{
	/*cv::Mat inputImage= cv::imread("/home/cam/Desktop/fis-ocr.jpg");
	cv::imshow("Diplay Image", inputImage);*/
	string outText;
	//string imPath = "/home/cam/Desktop/fis-ocr.jpg";
	std::string utf8_text = path.toUtf8().constData();
	string imPath = utf8_text;
	// Create Tesseract object
	tesseract::TessBaseAPI *ocr = new tesseract::TessBaseAPI();

	// Initialize tesseract to use English (eng) and the LSTM OCR engine.
	ocr->Init("/usr/share/tesseract-ocr/tessdata/", "tur");
	// Set Page segmentation mode to PSM_AUTO (3)
	ocr->SetPageSegMode(tesseract::PSM_AUTO);

	// Open input image using OpenCV
	Mat im = cv::imread(imPath, IMREAD_COLOR);

	// Set image data
	ocr->SetImage(im.data, im.cols, im.rows, 3, im.step);

	// Run Tesseract OCR on image
	outText = string(ocr->GetUTF8Text());

	// print recognized text
	cout << outText << endl; // Destroy used object and release memory ocr->End();
	ocr->End();
	QString tmp = QString::fromStdString(outText);

	ui->plainTextEdit->setPlainText(tmp);
}

void MainWindow::connectDatabase()
{



	QString pwd = "/home/cam/qtprojects/OcrBasedBillRecognize/BillRecognizeDb";
	mydb.setDatabaseName(pwd);

	QMessageBox msg;
	// BAGLANILAMAZSA HATA BILDIR
	if(!mydb.open()) {
		msg.setWindowTitle("Hata");
		msg.setText("Veritabanı Bağlantısı Yapılamadı.");
		msg.setIcon(QMessageBox::Information);
		msg.exec();
	} /*else {
		QString sql = "SELECT *FROM TELEFON";
		listele(sql);
	}*/
}

void MainWindow::on_pshBtnSelectImage_clicked()
{
	QString filename= QFileDialog::getOpenFileName(this,"open a file","/home/cam/Desktop");
	QFile file(filename);

	if(!file.open(QFile::ReadOnly))
	{
		QMessageBox::warning(this,"title","file open not");
	}
	ui->lblImagePath->setText(filename);
	QPixmap pm(filename);
	ui->lblShowSelectedImage->setPixmap(pm);
	ui->lblShowSelectedImage->setScaledContents(true);
	function(filename);

}

void MainWindow::on_pshBtnConnectDatabase_clicked()
{
	connectDatabase();
	listele();
}
void MainWindow::listele()
{
	QSqlQuery query(mydb);
	QSqlQueryModel *model = new QSqlQueryModel();
	QString sql = "select SirketAdi from Bills";
	query.exec(sql);
	/*model->setQuery(query);
	//QString sirketAdi= query.value("SirketAdi").toString();
	ui->tableView->setModel(model);
//	ui->tableView->hideColumn(0);*/

	int idName = query.record().indexOf("SirketAdi");
	while (query.next())
	{
	   QSqlRecord record= query.record();
	   qDebug() <<"date : " << record.value("Tarih").toString();
	}
}
